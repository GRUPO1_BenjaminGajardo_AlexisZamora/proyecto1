#include "MQ7.h"
#define A_PIN 2
#define VOLTAGE 5 // Se define la alimentacion como 5v
MQ7 mq7(A_PIN, VOLTAGE); // inicializa el sensor MQ7
int pin = 8; // polvo
unsigned long duration;
unsigned long starttime;
unsigned long sampletime_ms = 1000; // tiempo de muestreo 1s
unsigned long lowpulseoccupancy = 0;
float ratio = 0;
float concentration = 0;
float lecturamq7 = 0;

void setup() {
  Serial.begin(9600);
  pinMode(pin,INPUT);
  Serial.println("Calibrando MQ7");
  mq7.calibrate();    // calcula R0
  Serial.println("Calibracion lista!");
  starttime = millis(); // tiempo actual
}

void loop() {
  duration = pulseIn(pin, LOW); // detecta el ancho del pulso en LOW (Logica Inversa)
  lowpulseoccupancy = lowpulseoccupancy+duration;

  if ((millis()-starttime) > sampletime_ms) // ejecutar cada 1s
  {
    polution();
    gas();
    starttime = millis();
  }
}

void polution() {
    ratio = lowpulseoccupancy/(sampletime_ms*10.0);  // porcentaje
    concentration = 1.1*pow(ratio,3)-3.8*pow(ratio,2)+520*ratio+0.62; // se usa la ecuación de la curva de especificaciones
    Serial.print("concentracion = ");
    Serial.print(concentration);
    Serial.print(" pcs/0.01cf  -  ");
    if (concentration < 1.0) {
     Serial.println("Es un ambiente sin polvo y sin humo"); 
    }
    if (concentration > 1.0 && concentration < 20000) {
     Serial.println("El ambiente es aceptable"); 
    }
    if (concentration > 20000 && concentration < 315000) {
     Serial.println("Humo detectado"); 
    }
    if (concentration > 315000) {
     Serial.println("Humo detectado y alta cantidad de particulas"); 
    }
    lowpulseoccupancy = 0;
}

void gas() {
    Serial.print("PPM = "); 
    lecturamq7 = mq7.readPpm();
    Serial.print(lecturamq7);
    Serial.print(" - ");
    
    if (lecturamq7 < 44) {
      Serial.println("Concentracion de monoxido nominal, todo esta bien");
    }
    
    if (lecturamq7 >= 44) {
      Serial.println("Cuidado, la concentracion de monoxido esta sobre el limite permitido");
    }

    if (lecturamq7 >= 200) {
      Serial.println("Alerta, la concentracion de monoxido es peligrosamente alta");
    }
    
    Serial.println(">");   // final paquete
  }
