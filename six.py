#!usr/bin/python
from RPi_GPIO_i2c_LCD import lcd
import serial
import Adafruit_DHT
import time

i2c_address = 0x27
lcdDisplay = lcd.HD44780(i2c_address)

lcdDisplay.backlight("on")
print('inicializando...')
  
class Ardu: #Declaración de clase Ardu
    
    def __init__(self, port, bauds): #Método Constructor: establece el puerto y la tasa de baudios          
        
        self.port = port      #Argumento Self (uno mismo) para el puerto
        self.bauds = bauds    #Argumento Self (uno mismo) para los baudios
        

    def getData(self): #Metodo getData (recibe la información de arduino y la muestra por pantalla)

        #objeto serial (de biblioteca serial) se le pasa el objeto Arduino con sus atributos port y bauds
        ser = serial.Serial(Arduino.port, Arduino.bauds) 
        ser.flushInput() #descarta lo contenido en el buffer serial
        palabra = '' #se declara la variable   

        while True: #repetir en bucle
            #codigo a ejecutar, pero podrian presentarse errores
            try: 
                    #Lectura de datos que provienen del arduino, retorna un byte a la vez
                    readByte = ser.read()
                    #se decodifica por utf-8, convierte bytes de arduino a cadena de texto (string) 
                    line = readByte.decode('utf-8') 
                    
                    #completa la cadena de texto "palabra" con los valores de line      
                    palabra += line                 

                    #solo se ingresara a este bloque de codigo si el string palabra termina con ">"
                    if line == ">": 
                        
                        print(palabra)
                        sensorHT = Adafruit_DHT.DHT11
                        pinHT = 4
                        humedad, temperatura = Adafruit_DHT.read_retry(sensorHT, pinHT)                       
                        print('Temperatura={0:0.1f}° Humedad={1:0.1f}%'.format(temperatura, humedad))
                        if humedad > 70:
                            print('los niveles de humedad estan un poco altos, busque bajar del 70%')
                        elif humedad < 70:
                            print('la humedad es aceptable')
                        print('------------------------------------------------------------------------')    
                        
                        temp = (str(temperatura))
                        hum = (str(humedad))

                        temp = "Temp. [*C]:" + temp
                        hum = "Humedad [%]:" + hum  

                        lcdDisplay.set(temp, 1)
                        lcdDisplay.set(hum, 2)

                        time.sleep(1) 

                        palabra = "" #se reasigna el string palabra a ""

            except KeyboardInterrupt: #el programa se detiene con CTRL+C (interrupcion teclado)
                    print('saliendo...')
                    lcdDisplay.backlight("off")
                    break

Arduino = Ardu("/dev/ttyACM0", 9600) #Creación del objeto Arduino
Arduino.getData() #El objeto Arduino utiliza el método "getData()"